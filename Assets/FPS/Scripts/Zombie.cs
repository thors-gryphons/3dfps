using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    Vector3 direction;
    Transform playerTransform;
    Rigidbody rb;
    Animator animator;
    public float speed = 0.5f;
    HealthManager playerHealthManager;
    bool dealDamage;
    [SerializeField] float damage;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        playerTransform = GameObject.Find("Player").transform;
        playerHealthManager = playerTransform.GetComponent<HealthManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!playerTransform)
        {
            direction = Vector3.zero;
            return;
        }
        direction = playerTransform.position - transform.position;
        direction.y = 0;
        rb.rotation = Quaternion.LookRotation(direction);
        rb.angularVelocity = Vector3.zero;
        rb.MovePosition(rb.position + direction.normalized * speed * Time.deltaTime);
        animator.SetFloat("MoveSpeed", 1);
        if (dealDamage && playerHealthManager)
            playerHealthManager.TakeDamage(damage * Time.deltaTime);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
            dealDamage = true;
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.transform.tag == "Player")
            dealDamage = false;
    }
}
