using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    Camera fpsCam;
    public ParticleSystem muzzleFlash;
    float damage;

    [SerializeField] float range = 100f;
    public float fireRate = 20;
    float nextTimeToFire = 0f;

    private void Awake()
    {
        fpsCam = Camera.main;
    }

    private void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            if (Time.time >= nextTimeToFire)
            {
                nextTimeToFire = Time.time + 1 / fireRate;
                Shoot();
            }
        }

        void Shoot()
        {
            RaycastHit rayCastHit;
            if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.up, out rayCastHit, range))
            {
                Debug.Log(rayCastHit.transform.name);
                HealthManager hm = rayCastHit.transform.GetComponent<HealthManager>();
                // If we hit an object with a HealthManager
                if (hm)
                {
                    // Deal damage
                    hm.TakeDamage(damage);
                }
            }
            muzzleFlash.Play();
        }
    }
}
